# bash script


wget -O - https://gitlab.com/edu5920529/script/-/raw/main/10_ubuntu_docker_install.sh | bash

wget -O - https://gitlab.com/edu5920529/script/-/raw/main/20_ubuntu_gitlab-runner_install.sh | bash

wget -O - https://gitlab.com/edu5920529/script/-/raw/main/30_gitlab-runner_register.sh | bash

wget -O - https://gitlab.com/edu5920529/script/-/raw/main/40_ubuntu2004_kubeadm.sh | bash

wget -O - https://gitlab.com/edu5920529/script/-/raw/main/50_helm3_install.sh | bash

wget -qO - https://gitlab.com/edu5920529/script/-/raw/main/60_useradd.sh | bash -s username

wget -q -O - https://gitlab.com/edu5920529/script/-/raw/main/60_useradd.sh | xargs -d "\n" -a file_with_username bash /dev/stdin
