#!/bin/bash
sudo gitlab-runner register --non-interactive --url "https://gitlab.com" --registration-token "$(< gitlab-runner.token)" --description "Gitlab-runner" --executor "shell"
# wget -O - https://gitlab.com/edu5920529/script/-/raw/main/30_gitlab-runner_register.sh | bash
