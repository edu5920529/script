#!/bin/bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner -y
echo "gitlab-runner  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/gitlab-runner
sudo rm /home/gitlab-runner/.bash_logout
sudo gitlab-runner start
# wget -O - https://gitlab.com/edu5920529/script/-/raw/main/20_ubuntu_gitlab-runner_install.sh | bash
