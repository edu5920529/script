#!/bin/bash
USER=$1
sudo useradd -s /bin/bash -m $USER
sudo usermod -aG root $USER

ssh-keygen -t ed25519 -f $USER -q -N ""

sudo mkdir -p /home/$USER/.ssh
sudo cp $USER.pub /home/$USER/.ssh/authorized_keys
sudo chown -R $USER:$USER /home/$USER
sudo chmod go-rwx /home/$USER/.ssh/authorized_keys

echo "$USER  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/$USER

# wget -qO - https://gitlab.com/edu5920529/script/-/raw/main/60_useradd.sh | bash -s username
# wget -q -O - https://gitlab.com/edu5920529/script/-/raw/main/60_useradd.sh | xargs -d "\n" -a file_with_username bash /dev/stdin
